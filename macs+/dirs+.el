;;; dirs+.el -*- lexical-binding: t; -*-

;; dirs
(defvar dirs+ '(macs+ templates+ config+) "Emacs dirs")
(defvar dirs+system '(sync .config .cache .ssh .gnupg) "System dirs.")

(defun dirs+ (dirs prefix)
  (dolist (dir dirs)
    (let* ((dir-name (symbol-name dir))
           (fun (intern (concat "dirs+" dir-name))))
      (defalias fun
        (lambda (&rest args)
          (apply #'concat prefix dir-name "/" args))
        (format "Return ARGS subdirectory of %s."
                (concat prefix dir-name "/"))))))

(dirs+ dirs+ user-emacs-directory)
(dirs+ dirs+system "~/")

(provide 'dirs+)
;;; dirs+.el ends here
