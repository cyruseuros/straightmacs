;;; data+.el -*- lexical-binding: t; -*-

(use-package yaml-mode
  :gfhook #'highlight-indent-guides-mode
  :defer t)
(use-package json-mode :defer t)

(provide 'data+)
;;; json+.el ends here
