;;; persp+.el -*- lexical-binding: t; -*-

(use-package persp-mode
  :config
  (after+ 'hercules
    (hercules-def
     :toggle-funs #'persp+
     :keymap 'persp-key-map
     :transient t))
  (kbd+ "p." #'persp+)
  (persp-mode +1))

(provide 'persp+)
;;; persp+.el ends here
