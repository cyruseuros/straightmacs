;;; completion+.el -*- lexical-binding: t; -*-

(use-package compdef
  :defer t
  :straight (compdef
             :type git
             :host gitlab
             :repo "jjzmajic/compdef"
             :branch "develop"))

(use-package company
  :init (setq company-idle-delay nil)
  :config (global-company-mode))

(use-package helm-company
  :after helm+
  :config
  ;; emulate (setq tab-always-indent 'complete)
  (setq-default tab-always-indent t)
  (advice+ #'indent-for-tab-command
           :after-until #'helm-company))

(provide 'completion+)
;;; company+.el -*- lexical-binding: t; -*-
