;;; shell+.el -*- lexical-binding: t; -*-

(use-package fish-mode
  :init (setq fish-enable-auto-indent t)
  :defer t)

(use-package company-shell
  :after company sh-script compdef
  :config
  (after+ 'compdef
    (compdef
     :modes #'sh-mode
     :capf '(sh-completion-at-point-function)
     :company '(company-fish-shell
                company-shell-env
                company-files
                company-capf
                company-dabbrev-code))
    (compdef
     :modes #'sh-mode
     :capf '(sh-completion-at-point-function)
     :company '(company-shell
                company-shell-env
                company-files
                company-capf
                company-dabbrev-code))))

(provide 'shell+)
;;; shell+.el ends here
