;;; helm+.el -*- lexical-binding: t; -*-

(defvar helm+sandbox "~/sandbox/")

;;; funs
(defun helm+faces nil
  "Describe face with `helm'."
  (interactive)
  (require 'helm-elisp)
  (let ((default (or (face-at-point) (thing-at-point 'symbol))))
    (helm :sources (helm-def-source--emacs-faces
                    (format "%s" (or default "default")))
          :buffer "*helm faces*")))

(defun helm+sandbox nil
  "Got to sandbox directory"
  (interactive)
  (helm-find-files-1 helm+sandbox))

(defun helm+ag-dir nil
  (interactive)
  (helm-do-ag (file-name-directory buffer-file-name)))

(use-package helm
  :init
  (setq helm-show-completion-display-function nil
        helm-display-buffer-default-height 0.35
        helm-default-display-buffer-functions
        '(display-buffer-in-side-window))
  :config
  (helm-mode +1)
  (advice+ #'helm-locate-update-mode-line
           :override #'ignore)
  :general
  (general-def [remap execute-extended-command] #'helm-M-x)
  (general-def :keymaps 'helm-map :package 'helm
    [tab] #'helm-execute-persistent-action
    [backtab] #'helm-select-action)
  (kbd+
    "k" '(helm-show-kill-ring :wk "kill ring")
    "SPC" '(helm-M-x :wk "extended command")
    kbd+localleader '(helm-resume :wk "resume action")
    "ss" '(helm-occur :wk "search buffer")
    "bb" '(helm-mini :wk "list buffers"))
  (kbd+ :infix "f"
    "f" '(helm-find-files :wk "find file")
    "h" '(helm-recentf :wk "file history")
    "r" '(helm-find :wk "find recursive")
    "l" '(helm-locate :wk "locate file")))

(use-package helm-ag
  :init
  (setq-default
   helm-ag-insert-at-point 'symbol)
  :config
  (advice+
   #'helm-ag-show-status-default-mode-line
   :override #'ignore)
  :general
  (kbd+local :keymaps 'helm-ag-edit-map
    "," '(helm-ag--edit-commit :wk "commit edits")
    "c" '(helm-ag--edit-abort :wk "cancel edits"))
  (kbd+ :keymaps 'helm-ag-map
    "e" '(helm-ag-edit :wk "edit search"))
  (kbd+ :infix "j"
    "j" '(helm-imenu :wk "jump")
    "o" '(helm-imenu-in-all-buffers :wk "jump other"))
  (kbd+ :infix "s"
    "f" '(helm-do-ag-this-file :wk "search file")
    "p" '(helm-do-ag-project-root :wk "search project")
    "b" '(helm-do-ag-buffers :wk "search buffers")))

;;; fuzzy
(use-package fuz
  :config
  :disabled t
  (unless (require 'fuz-core nil t)
    (fuz-build-and-load-dymod))
  :if (executable-find "cargo"))
(use-package helm-fuz
  :config (helm-fuz-mode +1)
  :after fuz)

;;; calls
(kbd+
  "hi" '(helm-info-emacs :wk "helm info")
  "hs" '(helm+faces :wk "search faces")
  "fb" '(helm+sandbox :wk "find sandbox")
  "sd" '(helm+ag-dir :wk "search directory"))

(provide 'helm+)
;;; helm+.el ends here
