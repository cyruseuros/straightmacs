;;; rust+.el -*- lexical-binding: t; -*-

(use-package rustic
  :gfhook ('rustic-mode-hook
           '(flycheck-mode
             lsp-deferred))
  :init (setq rustic-lsp-server 'rls)
  :general (kbd+local :keymaps 'rustic-mode-map
             kbd+localleader #'rustic-popup
             "f" #'rustic-rustfix
             "c" #'rustic-cargo-clippy))

(after+ 'handle
  (handle #'rustic-mode
          :gotos #'lsp-find-definition
          :docs #'lsp-describe-thing-at-point
          :compilers #'rustic-compile
          :formatters #'rustic-format-buffer))

(provide 'rust+)
;;; rust+.el ends here
